#include "socks4.h"
#include "conn.h"

#define LISTEN_HOST "0.0.0.0"
#define LISTEN_PORT 1080
#define BACKLOG 10

#if 1
#define __DEBUG__
#endif

#define BUFSIZE 1024 
#define TIMEOUT_SEC 3600
#define CONFIG_FILE "socks.conf"

#define LIMIT 1024

/* main */
int main () {
    int msock = 0; /* master socket */
    int csock = 0; /* client socket */
    struct sockaddr_in addr;
    socklen_t addrlen;
    pid_t pid;
    struct acl* acl;

    fprintf(stderr, "Loading ACL....");
    acl = load_config(CONFIG_FILE);
    //list_acl_rules(acl);
    if (!acl) {
        fprintf(stderr, "\n");
        return (EXIT_FAILURE);
    }
    fprintf(stderr, "(done)\n");

    msock = passiveTCP(LISTEN_HOST, LISTEN_PORT, BACKLOG);
    
    signal(SIGCHLD, SIG_IGN);
    while ( (csock = accept(msock, (struct sockaddr*) &addr, &addrlen)) ) {
        pid = fork();

        if (pid > 0) {
            continue;
        } else if (pid == 0 ) {
            fprintf(stderr, "Connection from [%s:%d]\n", \
                    inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
            handle_request(csock, &addr, acl);
            shutdown(csock, SHUT_RDWR);
            close(csock);
            exit(0);
        } else {
            perror("pid()");
            continue;
        }
    }

    return (EXIT_SUCCESS);
}

void handle_request(int sock, struct sockaddr_in* addr, struct acl* acl) {
    char buf[BUFSIZE];
    int len = 0;
    struct socks4_header *header;
    struct socks4_header rheader;
    struct in_addr ia;
    int rsock = 0;
    fd_set rfds;
    fd_set wfds;
    struct timeval timeout;
    char *domain;
    int use_dns = 0;
    struct sockaddr_in bind_addr;
    struct sockaddr_in raddr;
    int bsock;
    int ret;
    enum action action;
    int cd;

    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    bzero(buf, BUFSIZE);
    bzero(&header, sizeof(header));
    bzero(&rheader, sizeof(rheader));
    len = recv(sock, buf, BUFSIZE, 0);
    if (len < sizeof(struct socks4_header)) {
        fprintf(stderr, "ERROR: Only receive %d bytes instead of %ld bytes for header.\n", \
            len, sizeof(struct socks4_header));
        goto cleanup;
    }

    header = (struct socks4_header*) buf;
    domain = ((char*) &(header->USERID)) + strlen( (char*) &(header->USERID) ) +1;
    ia.s_addr = header->DSTIP;
    if (strstr(inet_ntoa(ia), "0.0.0.") == inet_ntoa(ia)) {
        if (strlen(domain) > 0) {
            use_dns = 1;
        } else {
            goto cleanup;
        }
    }

    #ifdef __DEBUG__
    fprintf(stderr, "DEBUG: VN=%d, CD=%d, " \
        "DSTPORT=%d, DSTIP=%s, USERID=%s, "
        "DOMAIN=%s\n", \
        header->VN, header->CD, ntohs(header->DSTPORT), \
        inet_ntoa(ia), &header->USERID, domain);
    #endif
        

    if (header->VN != SOCKS4 ) {
        fprintf(stderr, "ERROR: Invalid version %d.\n", header->VN);
        shutdown(sock, SHUT_RDWR);
        close(sock);
        return;
    }
    
    cd = header->CD;
    if (header->CD==SOCKS4_CONNECT) {
        /* CONNECT Mode */
        if (use_dns) {
            rsock = connectTCP(domain, ntohs(header->DSTPORT));
        } else {
            rsock = connectTCP(inet_ntoa(ia), ntohs(header->DSTPORT));
        }

    } else if (header->CD==SOCKS4_BIND) {
        /* BIND Mode */
        bsock = passiveTCP("0.0.0.0", 0, BACKLOG);
        if (bsock < 0) goto cleanup;

        if (getsockname(bsock, (struct sockaddr*)&bind_addr, (socklen_t*)&len) < 0) {
            perror("getsockname()");
            goto cleanup;
        }
        
        rheader.DSTPORT = bind_addr.sin_port;
        rheader.DSTIP = bind_addr.sin_addr.s_addr;
        rheader.CD = SOCKS4_REQUEST_GRANTED;

        sendall(sock, &rheader, sizeof(rheader)-1, 0);
        rsock = accept(bsock, (struct sockaddr*) &raddr, (socklen_t*)&len);
        if (rsock < 0) perror("accept()");
    } else {
        fprintf(stderr, "ERROR: Invalid CD=%d\n", cd);
        goto cleanup;
    }

    /* 
       failed to CONNECT to destination 
       or failed to accept connection from remote host
    */
    if (rsock < 0) {
        rheader.VN = 0;
        rheader.CD = SOCKS4_REQUEST_REJECTED_OR_FAILED;
        goto cleanup;

    } else if (cd==SOCKS4_CONNECT) {
        rheader.VN = 0;
        // TODO: Add ACL
        /* acl */
        action = check_rules(&addr->sin_addr, header, acl);
        if (action == permit) {
            fprintf(stderr, "Request Granted.\n");
            rheader.CD = SOCKS4_REQUEST_GRANTED;
        } else {
            fprintf(stderr, "Request Rejected.\n");
            rheader.CD = SOCKS4_REQUEST_REJECTED_OR_FAILED;
        }

    } else if (cd==SOCKS4_BIND) {
        rheader.VN = 0;

        if (raddr.sin_addr.s_addr == header->DSTIP) {
            action = check_rules(&addr->sin_addr, header, acl);
            if (action == permit) {
                fprintf(stderr, "Request Granted.\n");
                rheader.CD = SOCKS4_REQUEST_GRANTED;
            } else {
                fprintf(stderr, "Request Rejected.\n");
                rheader.CD = SOCKS4_REQUEST_REJECTED_OR_FAILED;
            }
        } else {
            rheader.CD = SOCKS4_REQUEST_REJECTED_OR_FAILED;
            goto cleanup;
        }

        sendall(sock, &rheader, sizeof(rheader)-1, 0);
    }

    sendall(sock, &rheader, sizeof(rheader)-1, 0);

    #ifdef LIMIT
    int bind_sent = 0;
    #endif
    while (1) {
        timeout.tv_sec = TIMEOUT_SEC;
        timeout.tv_usec = 0;

        FD_ZERO(&rfds);
        FD_SET(sock, &rfds);
        FD_SET(rsock, &rfds);
        bzero(buf, BUFSIZE);

        ret = select( (1+(sock>rsock?sock:rsock)), &rfds, NULL, NULL, &timeout);
        if (ret < 0) {
            perror("select()");
            goto cleanup;
        }

        /* 
           read from client sock and write to remote sock
        */
        if ( (FD_ISSET(sock, &rfds)) ) {
            len = recv(sock, &buf, BUFSIZE, 0);

            if (len < 0) {
                perror("recv()");
                break;
            } else if (len == 0) {
                break;
            } else {
                sendall(rsock, buf, len, 0);
            }
        }

        /* 
           read from remote sock and write to client sock
        */
        if ( (FD_ISSET(rsock, &rfds)) ) {
            len = recv(rsock, buf, BUFSIZE, 0);

            if (len < 0) {
                perror("recv()");
                break;
            } else if (len == 0) {
                break;
            } else {
                #ifndef LIMIT
                sendall(sock, buf, len, 0);
                #else
                
                if (cd == SOCKS4_BIND) {
                    bind_sent += (sendall(sock, buf, len>LIMIT?LIMIT:len, 0));
                    if (bind_sent >= LIMIT) {
                        goto cleanup;
                    }
                } else if (cd == SOCKS4_CONNECT) {
                    sendall(sock, buf, len, 0);
                } else {
                    fprintf(stderr, "Invalid CD=%d.\n", cd);
                    goto cleanup;
                }
                #endif
            }
        }
    }
    
    /* disconnect */
    cleanup:
    fprintf(stderr, "Client [%s:%d] disconnected\n", inet_ntoa(addr->sin_addr),
            ntohs(addr->sin_port));
    if (cd == SOCKS4_BIND) {
        shutdown(bsock, SHUT_RDWR);
        close(bsock);
    }
    shutdown(sock, SHUT_RDWR);
    shutdown(rsock, SHUT_RDWR);
    close(sock);
    close(rsock);
}

