CFLAGS=-g -Wall

all: socks4
socks4: socks4.c socks4.h conn.o misc.o rule.o
	$(CC) $(CFLAGS) -o socks4 socks4.c conn.o misc.o rule.o
conn.o: conn.c conn.h
	$(CC) $(CFLAGS) -c -o conn.o conn.c
rule.o: rule.c rule.h misc.o
	$(CC) $(CFLAGS) -c -o rule.o rule.c misc.o
misc.o: misc.c misc.h
	$(CC) $(CFLAGS) -c -o misc.o misc.c
clean:
	rm -rf *.o *.core socks4
