#include "rule.h"

struct acl* default_rule() {
    /* default rule is DENY ALL */
    struct acl* default_rule;
    char *wildcard = "*";

    default_rule = (struct acl*) calloc(1, sizeof(struct acl));
    if (!default_rule) {
        perror("malloc()");
        return NULL;
    }

    default_rule->action = deny;
    default_rule->type = any;
    strncpy(default_rule->userid, wildcard, 254);
    strncpy(default_rule->src_ip, wildcard, 15);
    strncpy(default_rule->dst_host, wildcard, 254);
    strncpy(default_rule->dst_port, wildcard, 5);
    default_rule->next_rule = NULL;

    return default_rule;
}

struct acl* load_config(char* filename) {
    FILE *fp = NULL;
    char line[LINE_BUF_SIZE];
    char *rule;
    char *token;
    char *saveptr;
    char *delim = " \t";
    int token_counter = 0;
    struct acl* acl = NULL; 
    struct acl* new_acl = NULL; 
    struct acl* front = NULL;

    bzero(line, LINE_BUF_SIZE);
    fp = fopen(filename, "r");
    if (!fp) {
        perror("fopen()");
        return NULL;
    }

    /* create default policy acl */
    //acl = default_rule();
    //if (!acl) {
    //    return NULL;
    //}

    while ((rule = fgets(line, LINE_BUF_SIZE-1, fp))) {
        token_counter = 0;
        token = NULL;
        rule = trim(rule);

        if (strlen(rule) == 0 || *rule == '#') {
            continue; // bypass empty line and comment
        } else if ( *rule == '[' ) {
            // NOT IMPLEMENTED YET
            continue; // config block separator
        } else {
            new_acl = (struct acl*) calloc(1, sizeof(struct acl));
            new_acl->next_rule = NULL;
            if (!acl) {
                /* first acl */
                acl=new_acl;
                front = new_acl;
            } else {
                acl->next_rule = new_acl;
                acl = new_acl;
            }

            token = strtok_r(rule, delim, &saveptr);
            if (!token) {
                fprintf(stderr, "Syntax error: %s\n", rule);
                goto cleanup_acl;
            }
            ++token_counter;

            /* parse action */
            if (!strcmp(token, "permit")) {
                new_acl->action = permit;
            } else if (!strcmp(token, "deny"))  {
                new_acl->action = deny;
            } else {
                fprintf(stderr, "Unknown action \"%s\".\n", token);
                goto cleanup_acl;
            }

            /* parse type */
            token = strtok_r(NULL, delim, &saveptr);
            if (!token) {
                fprintf(stderr, "Syntax error: %s\n", rule);
                goto cleanup_acl;
            }
            ++token_counter;
            if (!strcmp(token, "*")) {
                new_acl->type = any;
            } else if (!strcmp(token, "connect")) {
                new_acl->type = _connect;
            } else if (!strcmp(token, "bind")) {
                new_acl->type = _bind;
            } else {
                fprintf(stderr, "Unknown type: %s\n", token);
                goto cleanup_acl;
            }

            /* copy userid */
            token = strtok_r(NULL, delim, &saveptr);
            if (!token) {
                fprintf(stderr, "Syntax error: %s\n", rule);
                goto cleanup_acl;
            }
            ++token_counter;
            strncpy(new_acl->userid, token, 254);

            /* copy source ip */
            token = strtok_r(NULL, delim, &saveptr);
            if (!token) {
                fprintf(stderr, "Syntax error: %s\n", rule);
                goto cleanup_acl;
            }
            ++token_counter;
            strncpy(new_acl->src_ip, token, 15);

            /* copy destination ip */
            token = strtok_r(NULL, delim, &saveptr);
            if (!token) {
                fprintf(stderr, "Syntax error: %s\n", rule);
                goto cleanup_acl;
            }
            ++token_counter;
            strncpy(new_acl->dst_host, token, 254);

            /* copy destination port */
            token = strtok_r(NULL, delim, &saveptr);
            if (!token) {
                fprintf(stderr, "Syntax error: %s\n", rule);
                goto cleanup_acl;
            }
            ++token_counter;
            strncpy(new_acl->dst_port, token, 5);
        }
    }
    if (!feof(fp)) {
        perror("fgets()");
        goto cleanup_acl;
    }

    return front;

    cleanup_acl:
    if (front) {
        free_acl(&front);
    }
    return NULL;
}

enum action check_rules(const struct in_addr *s, \
    struct socks4_header* header, struct acl* acl) {

    struct acl* rule;
    const char *wildcard = "*";
    struct in_addr ta;
    char *domain;
    int rule_counter = 0;

    rule = acl;

    while (rule) {
        ++rule_counter;
        
        /* match type */
        if (rule->type == any || rule->type == header->CD) {
            goto match_userid;
        } else {
            goto unmatched;
        }

        match_userid:
        /* match userid */
        if (!strcmp(rule->userid, wildcard) || !strcmp(rule->userid, \
            (char*) &header->USERID)) {
            goto match_src_ip;
        } else {
            // USERID unmatched
            goto unmatched;
        }

        match_src_ip:
        if (!strcmp(rule->src_ip, wildcard) || \
            strstr(inet_ntoa(*s), rule->src_ip)) {
            goto match_dst_host;
        } else {
            goto unmatched;
        }

        match_dst_host:
        ta.s_addr = header->DSTIP;

        if (!strcmp(rule->dst_host, wildcard)) {
            goto match_dst_port;
        } else if (strstr(inet_ntoa(ta), "0.0.0.")) {
            domain = (char*)(&header->USERID) + strlen((char*)&header->USERID) + 1;
            if (strstr(domain, rule->dst_host) == domain) goto match_dst_port;
            else goto unmatched;
        } else if (strstr(inet_ntoa(ta), rule->dst_host) ) {
            goto match_dst_port;
        } else {

            goto unmatched;
        }

        match_dst_port:
        if (!strcmp(rule->dst_port, wildcard) || \
            htons(atoi(rule->dst_port)) == header->DSTPORT) {
            //fprintf(stderr, "match rule %d\n", rule_counter);
            return rule->action;
        } else {
            goto unmatched;
        }

        
        unmatched:
        rule = rule->next_rule;
        continue;
    }

    return deny;
}

void free_acl(struct acl** acl) {
    // DO NOTHING
    struct acl* current_rule;
    struct acl* next_rule;

    if (acl == NULL || *acl == NULL) {
        return;
    } else {
        current_rule = *acl;
        next_rule = current_rule->next_rule;
    }

    while (1) {
        free(current_rule);

        if (!next_rule) break;
        current_rule = next_rule;
        next_rule = current_rule->next_rule;
    }
    
    *acl = NULL;

    return;
}

/* for debug only */
void list_acl_rules(struct acl* acl) {
    struct acl* rule;

    rule = acl;

    while (rule) {
        fprintf(stderr, "[%s] %s %s %s %s %s\n", \
            (rule->action == permit?"PERMIT":"DENY"), \
            (rule->type == _connect? "CONNECT": rule->type == any? "ANY": "BIND"),
            rule->userid, rule->src_ip, rule->dst_host, rule->dst_port);

        rule = rule->next_rule;
    }
}
