#ifndef __SOCKS4_H__
#define __SOCKS4_H__
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include "rule.h"

/*
+----+----+----+----+----+----+----+----+----+----+....+----+
| VN | CD | DSTPORT |      DSTIP        | USERID       |NULL|
+----+----+----+----+----+----+----+----+----+----+....+----+
   1    1      2              4           variable       1
*/

#define SOCKS4 4
#define SOCKS4_CONNECT 1
#define SOCKS4_BIND 2
#define SOCKS4_REQUEST_GRANTED 90
#define SOCKS4_REQUEST_REJECTED_OR_FAILED 91
#define SOCKS4_REQUEST_REJECTED_CANNOT_CONNECT_TO_IDENTD 91
#define SOCKS4_REQUEST_REJECTED_DIFFER_USERID 93
struct acl;

struct socks4_header {
    uint8_t VN; // Version
    uint8_t CD; // Command
    uint16_t DSTPORT;
    uint32_t DSTIP;
    unsigned char USERID; // starting point USERID
} __attribute__ ((__packed__));


void handle_request(int sock, struct sockaddr_in*, struct acl*);
#endif
