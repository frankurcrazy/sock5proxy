#ifndef __RULE_H__
#define __RULE_H__
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include "misc.h"
#include "socks4.h"

#define LINE_BUF_SIZE 1024
#define SOCKS4_CONNECT 1
#define SOCKS4_BIND 2
struct socks4_header;

/* structure for firewall ACL */
enum action {
    permit,
    deny
};

enum type {
    _connect=SOCKS4_CONNECT,
    _bind=SOCKS4_BIND,
    any
};

struct acl {
    enum action action;
    enum type type;
    char userid[255];
    char src_ip[16];
    char dst_host[255];
    char dst_port[6];
    struct acl* next_rule;
};

struct acl* load_config(char* filename);
struct acl* default_rule();
enum action check_rules(const struct in_addr*, struct socks4_header*, struct acl*);
void free_acl(struct acl** acl);
void list_acl_rules(struct acl*);
#endif
